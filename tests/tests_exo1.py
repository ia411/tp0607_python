import unittest
from exo1 import ChangeProblem

class TestExo1(unittest.TestCase):

    def setUp(self):
        pass

    def testNextStates(self):
        cp = ChangeProblem()
        self.assertEqual(cp.nextStates([200, 10, 10, 10, 10, 10]), [[0, 9, 10, 10, 10, 10], [100, 10, 9, 10, 10, 10],
                                    [150, 10, 10, 9, 10, 10], [180, 10, 10, 10, 9, 10], [190, 10, 10, 10, 10, 9],])
        self.assertEqual(cp.nextStates([90, 10, 10, 10, 10, 10]), [[40, 10, 10, 9, 10, 10], [70, 10, 10, 10, 9, 10],
                                    [80, 10, 10, 10, 10, 9]])
        self.assertEqual(cp.nextStates([90, 10, 10, 10, 10, 0]), [[40, 10, 10, 9, 10, 0], [70, 10, 10, 10, 9, 0]])
        self.assertEqual(cp.nextStates([90, 0, 0, 0, 0, 0]), [])

    def tearDown(self):
        pass