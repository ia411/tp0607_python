class Node:
    def __init__(self, state, father):
        self.State = state
        self.Father = father

    def __repr__(self):
        return str(self.State)

class Solution:
    def __init__(self, solutionNode, extendedNodesNumber, ):
        self.SolutionNode = solutionNode
        self.setSolutionPath()
        self.PathLength = len(self.Path)
        self.ExtendedNodesNumber = extendedNodesNumber
    
    def __repr__(self):
        return "{0} noeuds parcourus pour trouver la solution suivante en {1} noeuds :\n{2}".format(
            self.ExtendedNodesNumber, self.PathLength, self.Path
        )

    def setSolutionPath(self):
        node = self.SolutionNode
        self.Path = [node.State]
        while node.Father:
            node = node.Father
            self.Path.insert(0, node.State)

class Solver:
    @staticmethod
    def BFS(problem, initial_state, final_state, occurence_test = False):
        queue = [Node(initial_state, None)]
        closed = []
        node_counter = 1
        while queue:
            current_node = queue.pop(0)
            if current_node.State == final_state:
                return Solution(current_node, node_counter)
            for state in problem.nextStates(current_node.State):
                if not occurence_test or state not in closed:
                    node_counter += 1
                    queue.append(Node(state, current_node))
                    if occurence_test:
                        closed.append(state)
        return None
        
    @staticmethod
    def DFS(problem, initial_state, final_state, occurence_test = False):
        queue = [Node(initial_state, None)]
        closed = []
        node_counter = 1
        while queue:
            current_node = queue.pop(0)
            if current_node.State == final_state:
                return Solution(current_node, node_counter)
            tmp_queue = []
            for state in problem.nextStates(current_node.State):
                if not occurence_test or state not in closed:
                    node_counter += 1
                    tmp_queue.insert(0, Node(state, current_node))
                    if occurence_test:
                        closed.append(state)
            for node in tmp_queue:
                queue.insert(0, node)
        return None
