from core import Solution, Node, Solver

class ChangeProblem:
    # Set of the available snacks in the vending machine
    VENDING_MACHINE = {
        "snickers" : 2.60,
        "mars" : 1.50,
        "coca" : 1.90,
        "haribo" : 4.00,
        "granola" : 1000.00
    }
    NB_SNACKS = 5

    # Set of the amount of each type of coin in the vending machine
    E2 = 10; E1 = 2; C50 = 6; C20 = 20; C10 = 9

    # match index of coin amount in a state and its value
    MATCH_CHANGE_LIST = {
        1 : 200,
        2 : 100,
        3 : 50,
        4 : 20,
        5 : 10
    }

    def nextStates(self, state):
        """Return all possible states considerating the parameter state as the actual state.

        Call reduceChange for each type of coin available.

        IN :
        state -- the actual state

        OUT :
        list of all possible next state.

        """
        next_states = []
        for change in range(1, 6):
            if state[change] > 0 and self.reduceChange(state, change) != []:
                next_states.append(self.reduceChange(state, change))
        return next_states

    def reduceChange(self, state, index_change):
        """Return a state  with the change due and the amount coin used decreased.

        IN :
        state -- the state to start from.
        index_change -- index of the type of coin used

        OUT : 
        the new state.

        """
        new_state = state.copy()
        for index, value in self.MATCH_CHANGE_LIST.items():
            if index_change == index and state[0] - value >= 0:
                new_state[0] -= value
                new_state[index] -= 1
                return new_state
        return []

class ChangeProblemSolution(Solution):
    def __init__(self, solution):
        self.SolutionNode = solution.SolutionNode
        self.Path = solution.Path
        self.PathLength = solution.PathLength
        self.ExtendedNodesNumber = solution.ExtendedNodesNumber
    
    def __repr__(self):
        return super().__repr__() + "\n" + self.printableResult()

    def calcResult(self):
        """Calculate the number of each coin used to give the change.

        IN :
        path -- the path find with BFS or DFS

        OUT : 
        coins_amount -- list of amount of each type of coin used
        
        """
        coins_amount = [0, 0, 0, 0, 0]
        for index_match, value in ChangeProblem.MATCH_CHANGE_LIST.items():
            for index_path in range(0, self.PathLength - 1):
                if self.Path[index_path][0] - self.Path[index_path + 1][0] == value:
                    coins_amount[index_match - 1] += 1
        return coins_amount

    def printableResult(self):
        """Return a printable result of the number of each type of coin used to give the change.

        Uses calcResult tu calculate the number of each type of coin used.

        IN :
        path -- the path find with BFS or DFS

        OUT : 
        printable_res -- result string to print
        
        """
        printable_res = "Monnaie rendue :\n"
        giveback_change = self.calcResult()
        for index_coin_amount in range(0, len(giveback_change)):
            if giveback_change[index_coin_amount] > 0:
                printable_res += " - {0} pièce(s) de {1} euro(s)\n".format(giveback_change[index_coin_amount], 
                                                                        ChangeProblem.MATCH_CHANGE_LIST[index_coin_amount + 1] / 100)
        return printable_res

class ChangeSolver(Solver):
    @staticmethod
    def BFS(problem, initial_state, final_state, occurence_test = False):
        queue = [Node(initial_state, None)]
        closed = []
        node_counter = 1
        while queue:
            current_node = queue.pop(0)
            if current_node.State[0] == final_state:
                return Solution(current_node, node_counter)
            for state in problem.nextStates(current_node.State):
                if not occurence_test or state not in closed:
                    node_counter += 1
                    queue.append(Node(state, current_node))
                    if occurence_test:
                        closed.append(state)
        return None
        
    @staticmethod
    def DFS(problem, initial_state, final_state, occurence_test = False):
        queue = [Node(initial_state, None)]
        closed = []
        node_counter = 1
        while queue:
            current_node = queue.pop(0)
            if current_node.State[0] == final_state:
                return Solution(current_node, node_counter)
            tmp_queue = []
            for state in problem.nextStates(current_node.State):
                if not occurence_test or state not in closed:
                    node_counter += 1
                    tmp_queue.insert(0, Node(state, current_node))
                    if occurence_test:
                        closed.append(state)
            for node in tmp_queue:
                queue.insert(0, node)
        return None
    
def parse_string_to_int(string):
    """Parse string value to int handling exceptions.

    IN:
    string -- string to parse
    OUT:
    value -- int value or string value error

    """
    try:
        value = int(string)
    except ValueError:
        value = string + " not an integer"
    return value

if __name__ == "__main__":

    cp = ChangeProblem()
    
    # Input of the amount of money
    print("==================================================\n")
    correctvalue = False 
    while not correctvalue:
        T = parse_string_to_int(input("Donnez une valeur de centimes multiple de 10 :\n"))
        if isinstance(T, int):
            correctvalue = True
            if T < 0 or T % 10 != 0:
                correctvalue = False
    print("\nVous avez donné {0} centimes.".format(T))
    print("==================================================\n")

    # Print of available snacks and their price using VENDING_MACHINE
    index = 0
    for snack, price in cp.VENDING_MACHINE.items():
        index += 1
        print("{0} - {1} : {2} €\n".format(index, snack, price))

    # Input to choose a snack
    correctvalue = False
    while not correctvalue:
        choice = parse_string_to_int(input("Que voulez-vous achetez ? (entrez le chiffre correspondant au produit)\n"))
        if isinstance(choice, int):
            correctvalue = True
            if choice > cp.NB_SNACKS or choice < 1:
                correctvalue = False
    snack = list(cp.VENDING_MACHINE.keys())[choice - 1]
    P = list(cp.VENDING_MACHINE.values())[choice - 1]
    print("\nVous avez choisi un {0} à {1} €.".format(snack, P))
    print("==================================================\n")

    # Set of the initial state using the chhange due and the amounts of available coins in the vending machine
    initial_state =  [T - P * 100, cp.E2, cp.E1, cp.C50, cp.C20, cp.C10]
    final_state = 0

    # Process
    # starts only if there is enough money
    if initial_state[0] < 0:
        print("\nCalcul impossible, il vous manque de la monnaie !\n")
    else:
        print("\nLa machine vous doit {0} euros(s).\n".format(initial_state[0] / 100))
        solver = ChangeSolver()
        print("==================================================")
        sol = solver.BFS(cp, initial_state, final_state)
        if(isinstance(sol, Solution)):
            print(ChangeProblemSolution(sol))
        else:
            print("BFS : pas de solution")
        print("==================================================")
        sol = solver.BFS(cp, initial_state, final_state, True)
        if(isinstance(sol, Solution)):
            print(ChangeProblemSolution(sol))
        else:
            print("BFS occurence : pas de solution")
        print("==================================================")
        print("==================================================")
        sol = solver.DFS(cp, initial_state, final_state)
        if(isinstance(sol, Solution)):
            print(ChangeProblemSolution(sol))
        else:
            print("DFS : pas de solution")
        print("==================================================")
        sol = solver.DFS(cp, initial_state, final_state, True)
        if(isinstance(sol, Solution)):
            print(ChangeProblemSolution(sol))
        else:
            print("DFS occurence : pas de solution")
        print("==================================================")